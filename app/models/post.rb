class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy # deletes all associated comments when a post is deleted

	validates_presence_of :title
	validates_presence_of :body
end
